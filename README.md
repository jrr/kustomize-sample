# Kustomize sample project

This is what it took for [https://kustomize.io](Kustomize) to finally make sense to me. It also shows a bit of what it can do.

```
deploy
├── base
│   ├── configmap.yaml
│   ├── deployment.yaml
│   ├── ingress.yaml
│   ├── kustomization.yaml
│   ├── namespace.yaml
│   ├── rbac.yaml
│   └── service.yaml
└── overlays
    ├── dev
    │   └── kustomization.yaml
    ├── prod
    │   └── kustomization.yaml
    ├── qa
    │   └── kustomization.yaml
    └── stage
        └── kustomization.yaml
```
